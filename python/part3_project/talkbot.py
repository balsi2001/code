from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import *

app = Flask(__name__)

# Channel Access Token
line_bot_api = LineBotApi('')
# Channel Secret
handler = WebhookHandler('')

# 監聽所有來自 /callback 的 Post Request
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

# 處理訊息
@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    if event.message.text == "恐怖":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡恐怖動漫的你，卯咪在這裡為你推薦屍體派對'))

    if event.message.text == "百合":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡百合動漫的你，卯咪在這裡為你推薦終將成為妳'))

    if event.message.text == "魔法":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡魔法動漫的你，卯咪在這裡為你推薦七大罪'))

    if event.message.text == "音樂":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡音樂動漫的你，卯咪在這裡為你推薦K-ON！輕音部'))

    if event.message.text == "戀愛":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡戀愛動漫的你，卯咪在這裡為你推薦多田君不戀愛'))

    if event.message.text == "搞笑":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡搞笑動漫的你，卯咪在這裡為你推薦調教咖啡廳'))

    if event.message.text == "家庭":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='喜歡家庭動漫的你，卯咪在這裡為你推薦隱瞞之事'))

    if event.message.text == "溫馨":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡溫馨動漫的你，卯咪在這裡為你推薦紫羅蘭永恆花園'))

    if event.message.text == "校園":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡治癒動漫的你，卯咪在這裡為你推薦月色真美'))

    if event.message.text == "歌舞":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡治癒動漫的你，卯咪在這裡為你推薦LoveLive!'))

    if event.message.text == "異世界":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡異世界動漫的你，卯咪在這裡為你推薦NO GAME NO LIFE 遊戲人生'))

    if event.message.text == "賭博":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡賭博動漫的你，卯咪在這裡為你推薦狂賭之淵'))

    if event.message.text == "冒險":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡冒險動漫的你，卯咪在這裡為你推薦刀劍神域'))

    if event.message.text == "動作":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡動作動漫的你，卯咪在這裡為你推薦一拳超人'))

    if event.message.text == "潛水":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡潛水動漫的你，卯咪在這裡為你推薦碧藍之海'))

    if event.message.text == "職場":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡職場動漫的你，卯咪在這裡為你推薦白箱'))

    if event.message.text == "奇幻":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡奇幻動漫的你，卯咪在這裡為你推薦斬！赤紅之瞳'))

    if event.message.text == "超能力":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡超能力動漫的你，卯咪在這裡為你推薦罪惡王冠'))

    if event.message.text == "懸疑":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡治癒動漫的你，卯咪在這裡為你推薦死亡筆記本'))

    if event.message.text == "機器人":
        line_bot_api.reply_message(event.reply_token,TextSendMessage(text='喜歡機器人動漫的你，卯咪在這裡為你推薦超时空要塞'))

    if event.message.text == "我不喜歡動漫":
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text='哎呀呀，真是可惜呢，卯咪討厭不喜歡動漫的人呢'))

    message = TextSendMessage(text=event.message.text)
    line_bot_api.reply_message(event.reply_token,TextSendMessage(text='您好我是卯咪，輸入你喜歡的風格讓我為您推薦適合的動漫'))



import os
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
