#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

FILE *fp = NULL, *out = NULL, *com = NULL, *de = NULL, *dec = NULL;

typedef struct tree{        //data->inputChar time->frequency c->lengthOfCode code->theCombinationOf(0+1)
    unsigned char data;
    int time;
    int c;
    struct tree *left ,*right ;
    unsigned char code[10000];
}Tree;

typedef struct coding{
    int number;
    int countdata;
    char coddata[100];
}Coding;

Coding codd[100000];  //store decompressed code;
int tcodd = 0;              //count codd

Tree *stack[100000];  //store all input
Tree *ans[128];     //the space to store ascii code , then point to that struct store its imformation

void inorderPrint(Tree* p)      //use inorder to put all element into code.txt
{
    if (p!=NULL){
        inorderPrint(p->left);
        if (p->data != '\0'){
			//printf ("%c",p->data);
            fprintf(out,"%d ",(int)p->data);
            for (int j=0;j < p->c;j++){
                fprintf ( out, "%c",p->code[j]);
            }
            fprintf(out,"\n");
        }
        inorderPrint(p->right);
    }
} 
void encode(Tree *e){
    if (e->left){       //if left child exist
        for (int i=0;i<e->c;i++){
            e->left->code[i] = e->code[i]; 
        }
        e->left->c = e->c+1;
        e->left->code[e->left->c-1] = '0';
        if (e->left->data!='\0')
            ans[(int)e->left->data] = e->left;
        encode(e->left);
    }
    if (e->right){       //if left child exist
        for (int i=0;i<e->c;i++){
            e->right->code[i] = e->code[i]; 
        }
        e->right->c = e->c+1;
        e->right->code[e->right->c-1] = '1';
        if (e->right->data!='\0')
            ans[(int)e->right->data] = e->right;
        encode(e->right);
    }
}
int createTree(int tindex){
    Tree *node = NULL;
    node = (Tree*)malloc(sizeof (Tree));
    node->left = stack[tindex-1];
    node->right= stack[tindex-2];
    node->c = 0;
    node->data = '\0';
    node->time = stack[tindex-1]->time + stack[tindex-2]->time;
    tindex--;
    stack[tindex-1] = node;
    node = NULL;
    return tindex;
}
sort(int tindex){        //bubble sort
    for (int i=0;i<tindex;i++){
        for (int j=i+1;j<tindex;j++){
            if (stack[i]->time < stack[j]->time){
                Tree *t = NULL;
                t = (Tree*)malloc(sizeof(Tree));
                t = stack[i];
                stack[i] = stack[j];
                stack[j] = t;
            }
        }
    }
}
void compress( char huff[] ){
    int comarray[100000] = {} ,  clen = 0;;                 //store the uncompress code
    for (int i=0;i<strlen(huff);i++){               //starting encode
        for (int j=0;j<ans[(int)huff[i]]->c;j++){
            if ( ans[(int)huff[i]]->code[j] == '0' )
                comarray[clen++] = 0;
            else if ( ans[(int)huff[i]]->code[j] == '1' )
                comarray[clen++] = 1;
        }
    }
    fprintf(out,"%d\n",clen);       //output the length of code
    if ((clen%8) != 0) 
        clen = clen-(clen%8)+8;
    for (int i=0;i<clen/8;i++){
        int call=0;
        for (int j=0;j<8;j++){
            if (comarray[i*8+j]==1)
                call += pow(2,j);
        }
        //printf ("%d ",call);	//test
        fprintf(com,"%c",call);
    }//puts("");//test
    fclose(com);
}
void decompress(Tree *dnode){
    //printf ("decompressIn\n");
    dec = fopen("compressed","r");

    int dehu[100000] , dlen = 0;      //store compressed.bin -> undepressed int (decimal)
    unsigned char x;
    int dans[100000] , dtop = 0;      //depressed number (binary)

    while (fread(&x,sizeof(char),1,dec)){
        dehu[dlen++] = (int) x;
    }
    
    

    for (int i=0;i<dlen;i++){
        int dx = dehu[i];
        for (int j=0;j<8;j++){
            dans[dtop++] = dx%2;
            dx/=2;
        }
    }
    
    
    
    FILE *read;     //code.txt
    read  = fopen("code.txt","r");
    char tdtop[1000] = {};
    fgets(tdtop,1000,read);
    dtop=0;     //the length of code
    for (int i=0;tdtop[i]!='\n';i++){                   //find the real length of code -> dtop
        dtop = dtop*10 + (int)(tdtop[i] - '0');
    }
    

    //start turn 01010101 to abcdefg

    char lll[1000];        //read code.txt

    for (int i=0;i<1000;i++){
        codd[i].number = 0;
        codd[i].coddata[0] = '\0';
    }
    
    while (!(feof(read))){
        fgets(lll,1000,read);
        int lllength=strlen(lll);
        int lllnow = 0;
        while (lll[lllnow]!=' '){
            codd[tcodd].number = codd[tcodd].number*10 + (int)(lll[lllnow]-'0');
            lllnow++;
        }lllnow++;
        int codetemp=0;
        while (lllnow<lllength-1){
            codd[tcodd].coddata[codetemp] = lll[lllnow];
            lllnow++;
            codetemp++;
        }codd[tcodd].countdata = codetemp;       //store the length of code

        tcodd++;
    }tcodd--;
    
    
    for (int i=0;i<tcodd;i++){			//put the short code forward
    	for (int j=i+1;j<tcodd;j++){
    		if (codd[i].countdata>codd[j].countdata){
    			Coding tc;
    			tc = codd[i];
    			codd[i] = codd[j];
    			codd[j] = tc;
			}
		}
	}

	
	
    int waiting[100] = {} , wait = 0;     //wait to found
    int success = 0;                        //found -> 0
    
    for (int i=0;i<dtop;i++){               //銝????????
        waiting[wait] = dans[i];
        wait+=1;
        for (int j=0;j<tcodd && success!=1;j++){
            for (int k=0;k<codd[j].countdata && codd[j].countdata==wait ;k++){  //銝?????????交摮?詨?脰?瘥?
                if ( ((int)(codd[j].coddata[k]-'0') ) == waiting[k]){           //撠?游?敺??撠?
                    if ( k==codd[j].countdata-1 ){
                        success = 1;
                        //printf ("found\n");
                        fprintf(de , "%c", codd[j].number);
                    }
                }
                else{           //蝺函Ⅳ銝??
                    break;
                }
            }
        }
        if (success == 1){
            wait = 0;
            success = 0;
        }
    }

    fclose(dec);
    fclose(de);
    fclose(read);
}

int main(void){
    char x[100000], hu[100000], decom[100000];     //x to record input, hu to input , decom to decompress
    int tx[100000], index=0;               //tx to record times of x , index record the number of stack
    for (int i=0;i<100000;i++){   //initialize
        hu[i]='0';
        x [i]='0';
        tx[i]=0;
    }
	
    fp = fopen("input.txt", "r");
    out = fopen("code.txt", "w");
    com = fopen("compressed","w");
    de = fopen("output.txt","w");

    int len=0;              //the length of input
    unsigned char HU;
    
    while (fread(&HU, sizeof(char), 1, fp)){
        hu[len++] = HU;
    }hu[len]='\0';
    
    /*
    //test
    for (int i=0;i<len;i++){
    	printf ("%c",hu[i]);
	}puts("");
	//test
	*/
	
    for (int i=0;i<len;i++){        //to calculate the times of input
        int sit=0;                  //determine x weather found or not
        for (int j=0;j<index;j++){
            if (hu[i]==x[j]){
                sit = 1;
                tx[j]++;
            }
        }
        if (sit==0){
            x [index] = hu[i];
            tx[index]++;
            index++;
        }
    }

    for (int i=0;i<index;i++){      //instack, wait to create tree
        stack[i] = (Tree*)malloc(sizeof(Tree));
        stack[i]->time = tx[i];
        stack[i]->data = x[i];
        stack[i]->left = NULL;
        stack[i]->right= NULL;
        stack[i]->c = 0;
    }
    
    
    if (index < 3){    //exception
        Tree *Node = (Tree*)malloc(sizeof(Tree));
        if (index == 1){
            Node->c = 0;
            Node->data = '\0';
            Node->time = stack[0]->time;
            Node->right = stack[0];
            Node->left = NULL;
        }
        else if (index == 2){
            Node->c = 0;
            Node->data = '\0';
            Node->time = stack[0]->time + stack[1]->time;
            Node->left = stack[1];
            Node->right = stack[0];
        }
        for (int i=0;i<128;i++) ans[i] = NULL;
        //start encoding
        encode(Node);
        //compress to compressed.bin
        compress(hu);
        //code.txt ans store ans[] array
        inorderPrint(Node);
        fclose(out);
        //decompress
        decompress(Node);
        fclose(fp);
        return 0;
    }

    while (index>=2){
        sort(index);
        index = createTree(index);
    }

    Tree *Node = (Tree*)malloc(sizeof(Tree));
    Node = stack[0];
    for (int i=0;i<128;i++) ans[i] = NULL;
    
    //start encoding
    encode(Node);
    //compress to compressed.bin
    compress(hu);
    //print tree
    inorderPrint(Node);
    fclose(out);
    //decompress
    decompress(Node);
    fclose(fp);
}
