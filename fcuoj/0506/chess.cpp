#include<stdio.h>
int t, m, n, ans, que[10][2], dis[105][105];
int h[8][2]={{-2,-1},{-2,1},{-1,-2},{-1,2},{1,-2},{1,2},{2,-1},{2,1}};
int king[8][2]={{-1,-1},{-1,0},{-1,1},{0,-1},{0,1},{1,-1},{1,0},{1,1}};
int start_x, start_y, end_x, end_y, cur_x, cur_y, nx, ny;
char arr[105][105];
int vis[105][105];
char str[500][500];
int sol(){
	que[0][0] = start_x;//初始化的x座標 ,迭代後會變為存放已經經過的x座標
	que[0][1] = start_y;//初始化的y座標 ,迭代後會變為存放已經經過的y座標
	//printf("que[%d][0]is%d\n",i,que[i][0]);
		//printf("que[%d][1]is%d\n",i,que[i][1]);
	dis[start_x][start_y] = 0;
	vis[start_x][start_y] = 1;
	for(int i=0, j=1 ; i<j ; i++){
		cur_x = que[i][0];
		cur_y = que[i][1];
		if(cur_x == end_x && cur_y == end_y){
			//	printf("cur_x is %d,cur_y is %d\n",cur_x,cur_y);
			return dis[end_x][end_y];
		}
		for(int k=0 ; k<8 ; k++){
			nx = cur_x + king[k][0];//國王行經的x座標 
			ny = cur_y + king[k][1];//國王行經的y座標 
			if(nx < 0 || nx >=m || ny < 0 || ny >= n) continue;
			if(!vis[nx][ny] && str[nx][ny] != '#'){
				vis[nx][ny] = 1;
				dis[nx][ny] = dis[cur_x][cur_y] + 1;
				que[j][0] = nx;
				que[j][1] = ny;
				/*printf("que[%d][0]is%d\n",i,que[j][0]);
				printf("que[%d][1]is%d\n",i,que[j][1]);
				printf("j is%d\n",j);*/
				j++;
			}
		}
	}
	return -1;
}

int main(){
	scanf("%d",&t);
	while(t--){
		scanf("%d %d ",&m,&n);
		for(int i=0 ; i<m ; i++){
			for(int j=0 ; j<n ; j++){
				str[i][j] = '.';
				vis[i][j] = 0;
			}
		}
		for(int i=0;i<m;i++)
			gets(str[i]);
		for(int i=0 ; i<m ; i++){
			for(int j=0 ; j<n ; j++){
				if(str[i][j] == 'A'){
					start_x = i;
					start_y = j;
				}
				else if(str[i][j] == 'B'){
					end_x = i;
					end_y = j;
				}
				else if(str[i][j] == 'Z'){
					str[i][j] = '#';
					for(int k=0 ; k<8 ; k++){
						nx = i + h[k][0];
						ny = j + h[k][1];
						if(nx >= 0 && nx < m && ny >= 0 && ny < n && str[nx][ny] == '.'){
							str[nx][ny] = '#';
						}
					}
				}
			}
		}
		ans = sol();
		if(ans == -1)
			puts("King Peter, you can't go now!");
		else
			printf("Minimal possible length of a trip is %d\n",ans);
	}
}
