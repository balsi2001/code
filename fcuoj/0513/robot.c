#include <stdio.h>
#include <stdlib.h>
int main() {
    int n, m, x, y;
    char D[2], arr[1000], tmp[100][100] = {};
    scanf("%d %d", &n, &m);
    while(scanf("%d %d %s", &x, &y, D) == 3) {
        scanf("%s", arr);
        int d = D[0], flag = 0, i;
        for(i = 0; arr[i]; i++) {
            if(arr[i] == 'F') {
                switch(d) {
                    case 'N':y++;break;
                    case 'E':x++;break;
                    case 'W':x--;break;
                    case 'S':y--;break;
                }
            } else if(arr[i] == 'R') {
                switch(d) {
                    case 'N':d = 'E';break;
                    case 'E':d = 'S';break;
                    case 'W':d = 'N';break;
                    case 'S':d = 'W';break;
                }
            } else {
                switch(d) {
                    case 'N':d = 'W';break;
                    case 'E':d = 'N';break;
                    case 'W':d = 'S';break;
                    case 'S':d = 'E';break;
                }
            }
            if(x < 0 || y < 0 || x > n || y > m) {
                switch(d) {
                    case 'N':y--;break;
                    case 'E':x--;break;
                    case 'W':x++;break;
                    case 'S':y++;break;
                }
                if(tmp[x][y] == 1)
                    continue;
                flag = 1;
                tmp[x][y] = 1;
                break;
            }
        }
        if(!flag)
            printf("%d %d %c\n", x, y, d);
        else {
            printf("%d %d %c LOST\n", x, y, d);
        }
    }
}
