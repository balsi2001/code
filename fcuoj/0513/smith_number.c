#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int count(int n) {
    char str[12];
    sprintf(str,"%d", n);
    int i, sum = 0;
    for(i = 0; str[i]; i++)
        sum += str[i]-'0';
    return sum;
}
int C(int n) {
    int i, ans;
    int x = count(n), y = 0, flag = 1;
    ans = n;
    for(i = 2; i <= (int)sqrt(n); i++) {
        if(n%i == 0) {
            int time = 0;
            while(n%i == 0) {
                time++;
                n /= i;
            }
            y += time*count(i);
            flag = 0;
        }
    }
    if(n != 1)
        y += count(n);
    if(x == y && flag == 0) {
        printf("%d\n", ans);
        return 1;
    }
    return 0;
}
int main() {
    int i;
    int T, n;
    scanf("%d", &T);
    while(T--) {
        scanf("%d", &n);
        for(i = n+1; ; i++)
            if(C(i) == 1)
                break;
    }
}
