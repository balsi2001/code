#include<stdio.h>

int main()

{

    int n,a,b,x,y,s;

    scanf("%ld",&n);

    while(n--)

    {

        scanf("%d%d",&a,&b);

        x=(a+b)/2;

        y=(a-b)/2;

        s=a+b;

        if(s%2!=0)

            printf("impossible\n");

        else if(x<0||y<0) printf("impossible\n");

        else

            printf("%d %d\n",x,y);

    }

}
