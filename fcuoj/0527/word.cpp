#include<stdio.h>
int sol(char *s){
	int cnt[26]={0},mcnt[26]={0};
    int diff=0,same=0;
    for(int i=0;s[i];i++)
        cnt[s[i]-'a']++;
        for(int i=0;i<26;i++) {
            if(cnt[i]) {
            if(!mcnt[cnt[i]])
                diff++;
                if(mcnt[cnt[i]]>0)
                    same=1;
                    mcnt[cnt[i]]++;
                }
            }
    if(diff>1&&!same)return 1;
    return 0;
}
int main() {
    int T,i=1;
    char s[32];
    while(scanf("%d", &T)!=EOF){
        int ans=0;
        for(int i=0;i<T;i++) {
            scanf("%s",s);
            if(sol(s))
               ans++;
        }
        printf("Case %d: %d\n",i,ans);
        i++;
    }
}
