#include<stdio.h>
int sol(int h1,int m1,int h2,int m2){
	h1*=60;
 	m1+=h1;
	h2*=60;
 	m2+=h2;
  int ans=m2-m1;
  if(ans<0)ans+=1440;
  return ans;
}
int main(){
  int h1,h2,m1,m2;
  while(scanf("%d%d%d%d",&h1,&m1,&h2,&m2)){
    if(!m1&&!m2&&!h1&&!h2)break;
    printf("%d\n",sol(h1,m1,h2,m2));
  }
}
