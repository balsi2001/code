#include<stdio.h>
int sol(char *num,int *S,int m){
	int ans=1;
	for (int i=0;i<m;i++) {
			int r = 0;
			for (int j=0;num[j];j++) {
				r=(r*10 + num[j]-'0')%S[i];
			}
			if(r!=0){
				ans=0;
				break;
			}
		}
	return ans;
} 
int main(){
	int T,a[12],l,m;
	char n[1200]={};
	while(~scanf("%d",&T))
	while(T--){
		l=0;
		scanf("%s%d",n,&m);
		for(int i=0;i<m;i++)
			scanf("%d",&a[i]);
		if(sol(n,a,m)==1){
			printf("%s - Wonderful.\n",n);
		}
		else{
			printf("%s - Simple.\n",n);
		}
	}
}
