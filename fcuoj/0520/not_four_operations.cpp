#include <stdio.h>
#include<math.h>
int la,lb,ra,rb,c=0;
int sol(char *str,int a,int b) {
    a=0,b=0;
    int g=0,f=0,neg=1;
    for(int i=0;str[i];i++) {
        if(str[i]=='x') {
            if(g)
                a+=neg*f;
            else
                a+=neg;
            g=0,f=0,neg=1;
        }else{
            if(str[i]=='-') {
                if(g)
                    b+=neg*f;
                g=0,f=0;
                neg=-1;
            }else if(str[i]=='+') {
                if(g)
                    b+=neg*f;
                g=0,f=0;
                neg=1;
            }else
                f=f*10+str[i]-'0',g=1;
        }
    }
    if(g)
        b+=neg*f;
    c++;
    if(c==1)la=a,lb=b;
    else ra=a,rb=b;
}
int main() {
    int T;
    scanf("%d", &T);
    char s1[502],*s2;
    while(T--) {
      	c=0;
        scanf("%s", s1);
        for(int i=0;s1[i];i++) {
            if(s1[i]=='=') {
                s2=s1+i+1;
                s1[i]='\0';
              //  printf("i is%d\n",i);
            }
        }
        //printf("s1 is %s\n",s1);
        //printf("s2 is %s",s2);
        //int la,lb,ra,rb;
        sol(s1,la,lb);
        sol(s2,ra,rb);
        if(la==ra&&lb==rb)
            puts("IDENTITY");
        else if(la==ra&&lb!=rb)
            puts("IMPOSSIBLE");
        else
            printf("%d\n", (int)floor((double)(rb-lb)/(la-ra)));
    }
}
