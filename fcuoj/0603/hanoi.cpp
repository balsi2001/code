#include <stdio.h> 
int moved=0;
int sol(int n, char A, char B, char C) {
    if(n == 1) {
        moved++;
    }
    else {
       	sol(n-1, A, C, B);
        sol(1, A, B, C);
        sol(n-1, B, A, C);
    }
}
int main() {
    int a,b,M=0,D=0,Y=0;
    while(scanf("%d%d",&a,&b)!=EOF){
    	sol(a, 'A', 'B', 'C');
		D=moved/b;
		Y=D/365;
		M=D/30-Y*12;
		D-=365*Y+30*M;
		if(D<0)M--,D+=30;
		printf("%d %d %d\n",Y,M,D);
		moved=0,M=0,D=0,Y=0;
	}
}
