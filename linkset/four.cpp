#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
struct Stack
{
    int top;
    unsigned capacity;
    int *array;
};

int cnt = 0;
struct Stack *createStack(unsigned capacity)
{
    struct Stack *stack = (struct Stack *)
        malloc(sizeof(struct Stack));

    if (!stack)
        return NULL;

    stack->top = -1;
    stack->capacity = capacity;

    stack->array = (int *)malloc(stack->capacity * sizeof(int));

    return stack;
}
int isEmpty(struct Stack *stack)
{
    return stack->top == -1;
}
char peek(struct Stack *stack)
{
    return stack->array[stack->top];
}
char pop(struct Stack *stack)
{
    if (!isEmpty(stack))
        return stack->array[stack->top--];
    return NULL;
}
void push(struct Stack *stack, char op)
{
    stack->array[++stack->top] = op;
}

int isOperand(char ch)
{	
if(((ch>=32&&ch<=39)||ch==44||ch>=93||(ch>=95&&ch<=127)))
puts("輸入非數字或運算子"),exit(1);
    return (ch >= '0' && ch <= '9');
}

// 回傳數字越大代表越先處理計算
int Prec(char ch)
{
    switch (ch)
    {
    case '+':
    case '-':
        return 1;
        cnt++;

    case '*':
    case '/':
        return 2;
        cnt++;

    case '^':
        return 3;
    }
    return -1;
}

int infixToPostfix(char exp[])
{
    int i, k;

    struct Stack *stack = createStack(strlen(exp));
    if (!stack)
        return -1;
    int a = 0;
    for (i = 0, k = -1; exp[i]; ++i)
    {

        if (isOperand(exp[i])) //判斷是否是運算符號
            exp[++k] = exp[i];

        else if (exp[i] == '(')
            push(stack, exp[i]), a++;

        else if (exp[i] == ')')
        {
            a--;
            while (!isEmpty(stack) && peek(stack) != '(')
                exp[++k] = pop(stack);
            if (!isEmpty(stack) && peek(stack) != '(')
                return -1;
            else
                pop(stack);
        }
        else
        {
            while (!isEmpty(stack) &&
                   Prec(exp[i]) <= Prec(peek(stack)))
                exp[++k] = pop(stack);
            push(stack, exp[i]);
        }
    }
    if (a != 0)
        puts("括號格式不正確"), exit(1);
    while (!isEmpty(stack))
        exp[++k] = pop(stack);
    exp[++k] = '\0';
}
int evaluatePostfix(char exp[]) //取出符號並運算
{
    struct Stack *stack = createStack(strlen(exp));
    int i;
    int num = 0, cnt = 0;
    if (!stack)
        return -1;

    for (i = 0; exp[i]; ++i)
    {
        if (isdigit(exp[i])) //是數字就-字元 0 轉型
            push(stack, exp[i] - '0'), num++;

        else
        { //如果 exp 此時不是數字，就從 stack pop 出兩個值出來運算
            int val1 = pop(stack);
            int val2 = pop(stack);

            switch (exp[i])
            {
            case '+':
                push(stack, val2 + val1);
                break;
            case '-':
                push(stack, val2 - val1);
                break;
            case '*':
                push(stack, val2 * val1);
                break;
            case '/':
                push(stack, val2 / val1);
                break;
            case '^':
                push(stack, pow(val2, val1));
                break;
            }
            cnt++;
        }
    }
    if (num - 1 != cnt)
    {
        puts("運算子不匹配");
        exit(1);
    }
    else
        return pop(stack);
}
int main()
{
    char exp[100];
    gets(exp);
    if (!infixToPostfix(exp))
        puts("括號格式不正確");
    else
        printf("計算結果: %d", evaluatePostfix(exp));
}

