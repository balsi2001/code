#include<stdio.h>
#include<stdlib.h>
struct listNode{
	int val;
	struct listNode *next;
}; 
typedef struct listNode ListNode;
typedef ListNode *ListNodePtr;
struct listNode *rotateRight(ListNode *head, int k) {
    if (!head||k == 0) return head;
	struct listNode* leastNode = head;
	int n = 1;
	while (leastNode->next)
	{
		leastNode = leastNode->next;
		n++;
	}
	k = k%n;		
	if (k == 0) return head;
	k = n - k;
	leastNode->next = head;
	struct listNode *newHead = head;
	for (int i = 0; i < k - 1; i++)
		newHead = newHead->next;
	head = newHead->next;
	newHead->next = NULL;
	return head;
}
void insert(ListNodePtr *sPtr,int v){
	ListNodePtr newPtr=(ListNode*)malloc(sizeof(ListNodePtr));
			if(newPtr!=NULL){
			newPtr->val=v;
			newPtr->next=NULL;
			ListNodePtr pre=NULL;
			ListNodePtr cur=*sPtr;
			while(cur!=NULL&&v>cur->val){
				pre=cur;
				cur=cur->next;
			}
			if(pre==NULL){
				newPtr->next=*sPtr;
				*sPtr=newPtr;
			}
			else{
				pre->next=newPtr;
				newPtr->next=cur;
			}
		}
}
void print(ListNodePtr cur ){
	while(cur!=NULL){
		printf("%d ",cur->val);
		cur=cur->next;
	}
	puts("NULL");
}
int main(){
	int v;
	ListNodePtr startPtr=NULL;
	for(int i=0;i<5;i++){
		scanf("%d",&v);
	insert(&startPtr,v);
	}
	
	//rotateRight(startPtr,3);
	ListNodePtr s=rotateRight(startPtr,5);
	while(s){
		printf("%d ",s->val);
		s=s->next;
	}
}
